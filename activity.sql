SELECT *
  FROM artists
 WHERE name LIKE "%d%";

SELECT *
  FROM songs
 WHERE length < 230;

SELECT a.name AS album_name, b.title AS song_title, b.length
  FROM albums AS a
  JOIN songs AS b
    ON a.id = b.album_id;

SELECT *
  FROM artists AS a
  JOIN albums AS b
    ON a.id = b.artist_id
 WHERE b.name LIKE '%a%';

SELECT *
  FROM albums
  ORDER BY name DESC
  LIMIT 4;

SELECT *
  FROM albums AS a
  JOIN songs AS b
    ON a.id = b.album_id
  ORDER BY a.name DESC, b.title ASC;